FROM python:alpine

# This actually _disables_ the cache. (I know, right?)
ENV PIP_NO_CACHE_DIR=false

# Required for building C stuff apparently one of the dependencies needs.
RUN apk add build-base

WORKDIR /usr/src/app

COPY Pipfile.lock ./
RUN pip install pipenv \
	&& pipenv sync

COPY . ./

CMD [ "pipenv", "run", "python", "main.py" ]
