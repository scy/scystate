import asyncio
from os import environ as env

from scystate.jessie import Jessie
from scystate.mqtt import MQTT, MQTTConfigError
from scystate.slack import Slack
from scystate.state import MessengerStatusManager, StateManager


def docker_secrets_to_env():
    from os import environ
    from pathlib import Path
    try:
        for sf in Path("/run/secrets").iterdir():
            if sf.name not in environ and sf.name.isupper():
                try:
                    environ[sf.name] = sf.read_text()
                except Exception:
                    pass
    except Exception:
        pass

docker_secrets_to_env()


statemgr = StateManager()
msgr_sm = MessengerStatusManager(statemgr)

try:
    mqtt = MQTT()
except MQTTConfigError:
    print("MQTT not configured, disabling.")
    mqtt = None

jessie = Jessie(statemgr, mqtt) if mqtt else None

slack = Slack(env["SLACK_TOKEN"], msgr_sm) if "SLACK_TOKEN" in env else None
if slack is None:
    print("No SLACK_TOKEN, disabling Slack.")


async def main():
    if mqtt:
        mqtt.start()
    if jessie:
        asyncio.create_task(jessie.watch())
    while True:
        await asyncio.sleep(1)


if __name__ == "__main__":
    statemgr.on_update.append(lambda sm, vals: print("Update:", vals))
    asyncio.run(main())
