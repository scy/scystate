# scystate: What am I currently doing?

This is supposed to be some kind of software system that tracks various sources to find out what I personally am currently doing, and then publishes that to several places.

## Implemented

* Am I currently driving [Jessie](https://codeberg.org/scy/jessie), my van?

  To be fair, what it really checks is whether the van is moving continuously faster than a certain speed, respectively whether it has stopped moving again long enough.
  But there’s really rarely someone else driving.

* Take all the available signals (currently, that’s just, uhm, one) and condense them to a status message to be used in any messenger.

* Update my [Slack](https://slack.com/) status every time that messenger status changes.

  I’m in just one Slack workspace, so the code doesn’t support multiple workspaces yet.

## Planned

* Where am I right now (using my phone and reverse geocoding)?
* How’s the weather there (using [wttr.in’s JSON](https://github.com/chubin/wttr.in#json-output))?
* Am I in a phone call (using [Tasker](https://tasker.joaoapps.com/) or something)?
* Am I in a [Zoom](https://zoom.us/) call (probably using [Zoom’s presence webhook](https://marketplace.zoom.us/docs/api-reference/webhook-reference/user-events/presence-status-updated))?
* Do I have my Bluetooth headset connected? (Can be used for something like “yes, I’m driving, but you can call me anyway”.)
* When will I arrive at my destination? [OsmAnd](https://osmand.net/), which I’m using for navigation, might already do broadcast intents communicating this, but I’m not sure.
* Is my phone in “do not disturb” mode? This could then be set on other messengers, too.
* Am I currently sleeping? I’d probably implement this as a manual toggle on my phone.
* Am I gaming/coding/… (by recognizing applications running on my devices)?
* What am I listening to (using [Last.fm](https://www.last.fm/), and maybe my podcatcher [AntennaPod](https://antennapod.org/) could be convinced to share this, too)?
  * There are also some “now playing history” apps available that might help.

Some of these should totally end up in my Slack status.
But I can see it in other places, too:

* Update [@timwohnt](https://twitter.com/timwohnt)’s location every time I stop driving.
* Embed an overview of my state on my personal website.

## Setting up

This code is heavily tailored to me personally.
Nevertheless, if you’d like to try it out or customize it:

* Have at least Python 3.7.
* `pip install pipenv`
* `pipenv sync`
* Configure required environment variables:
  * `SLACK_TOKEN`: Slack user token with `users.profile:write` permissions.
  * `MQTT_HOST`: For connecting to the MQTT server providing location updates [like Jessie does](https://codeberg.org/scy/jessie/src/branch/main/src/gnss).
  * `MQTT_PORT`, `MQTT_USER`, `MQTT_PASS`: Self-explanatory, all optional.
* `pipenv run python3 main.py`

### Docker

This tool is also available as a Docker image: [`scy83/scystate`](https://hub.docker.com/r/scy83/scystate).

It is currently built semi-automatically (because Codeberg doesn't have CI/CD features yet), which means that the Docker version may lag behind this repo's `main` branch.
However, since the Docker version is what I'm using myself to deploy scystate, it shouldn't lag by a lot.

You'll need to provide the environment variables mentioned above nevertheless.
You can also provide them as secrets with the respective name (so that they end up in e.g. `/run/secrets/SLACK_TOKEN`).

## Contributing

I will probably **not** accept any code, since all of this is so heavily customized and I don’t need code that solves problems I don’t have.
If you think you have something that I could really benefit from, let me know.

Also, please **do** let me know if you have ideas on how to access some of the data listed in “[Planned](#planned)” above, e.g. how to get OsmAnd’s ETA and destination.

You can contact me [on Mastodon](https://mastodon.scy.name/@scy) [or Twitter](https://twitter.com/scy), or simply create an issue in this repo.
