import asyncio
import inspect
import traceback


class Event(list):
    """Simple event system.

    Inspired by <https://stackoverflow.com/a/2022629/417040>.
    """

    def __call__(self, *args, **kwargs):
        for cb in self:
            res = None
            try:
                res = cb(*args, **kwargs)
            except Exception:
                traceback.print_exc()
            if inspect.isawaitable(res):
                asyncio.create_task(res)
