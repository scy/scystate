import asyncio
from datetime import datetime, timedelta, timezone
import struct


"""State retrieval from Jessie.

Jessie, in case you're wondering, is my van. You can find more information
about it and the software running in it at <https://codeberg.org/scy/jessie>.
"""


class Jessie:
    TOPIC = "jessie/gnss/packed"
    PAYLOAD_LEN = 23

    MIN_SPEED = 5
    MIN_IN_MOTION = timedelta(seconds=10)
    MIN_STOPPED = timedelta(minutes=5)
    UNKNOWN_AFTER = timedelta(seconds=60)

    def __init__(self, statemgr, mqtt):
        self.over_min_speed_since = None
        self.under_min_speed_since = None
        self.last_speed_update_at = None
        self.driving = False
        self.statemgr = statemgr
        mqtt.on_connect.append(self.on_mqtt_connect)
        mqtt.on_message.append(self.on_mqtt_message)

    def on_mqtt_connect(self, mqtt, rc, flags):
        mqtt.subscribe(self.TOPIC)

    def on_mqtt_message(self, mqtt, topic, payload, qos, retain):
        if topic != self.TOPIC:
            return
        if len(payload) != self.PAYLOAD_LEN:
            return
        ts, lat, lon, alt, spd, crs, qs = struct.unpack("!dffeeeB", payload)

        # Special case: On the first message we see, instantly set the state.
        if self.over_min_speed_since is None and self.under_min_speed_since is None:
            self.set_driving(spd >= self.MIN_SPEED)

        now = datetime.now(tz=timezone.utc)
        if spd >= self.MIN_SPEED:
            self.under_min_speed_since = None
            if self.over_min_speed_since is None:
                self.over_min_speed_since = now
            if now - self.over_min_speed_since >= self.MIN_IN_MOTION and not self.driving:
                self.set_driving(True)
        else:
            self.over_min_speed_since = None
            if self.under_min_speed_since is None:
                self.under_min_speed_since = now
            if now - self.under_min_speed_since >= self.MIN_STOPPED and self.driving:
                self.set_driving(False)
        self.last_speed_update_at = now

    def set_driving(self, driving):
        self.driving = driving
        self.statemgr.update(driving_jessie=driving)

    async def watch(self):
        while True:
            await asyncio.sleep(1)
            if self.last_speed_update_at and datetime.now(tz=timezone.utc) - self.last_speed_update_at > self.UNKNOWN_AFTER and self.driving is not None:
                self.over_min_speed_since = None
                self.under_min_speed_since = None
                self.set_driving(None)
