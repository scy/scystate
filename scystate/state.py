from typing import Optional

from pydantic import BaseModel, Field

from .event import Event


class State(BaseModel):
    driving_jessie: Optional[bool]


class MessengerStatus(BaseModel):
    message: str = Field("")
    emoji: str = Field("")


class StateManager:
    def __init__(self):
        self._state = State()
        self.on_update = Event()

    def get_state(self) -> State:
        return self._state.copy()

    def update(self, **kwargs):
        for k, v in kwargs.items():
            self._state.__setattr__(k, v)
        self.on_update(self, kwargs)


class MessengerStatusManager:
    def __init__(self, statemgr: StateManager):
        self._status = None
        self.on_update = Event()
        statemgr.on_update.append(self._on_state_update)

    def _on_state_update(self, sm: StateManager, changed: dict):
        if "driving_jessie" in changed:
            self._decide(sm.get_state())

    def _decide(self, state: State):
        if state.driving_jessie:
            self._set_status(MessengerStatus(
                emoji="minibus", message="driving my van",
            ))
        else:
            self._set_status(MessengerStatus())

    def _set_status(self, new: MessengerStatus):
        if new != self._status:
            self._status = new
            self.on_update(new)
