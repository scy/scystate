import asyncio
from datetime import datetime, timedelta, timezone
from typing import Optional, Union

from pydantic import BaseModel, Field, validator
from slack_sdk.web.async_client import AsyncWebClient

from .state import MessengerStatus


_earliest_expire = datetime(2020, 1, 1, tzinfo=timezone.utc)
_no_expiration = datetime(1970, 1, 1, 0, 0, 0, tzinfo=timezone.utc)


class Status(BaseModel):
    emoji: str = Field("")
    text: str = Field("")
    expires: Union[bool, datetime] = Field(False)

    @validator("expires")
    def expires_false_or_date(cls, v):
        if v is True:
            raise ValueError("must be a datetime or False")
        if isinstance(v, datetime):
            if v == _no_expiration:
                return False
            if v < _earliest_expire:
                raise ValueError("expiration date too far in the past")
        return v

    @classmethod
    def from_profile(cls, profile: dict) -> "Status":
        return cls(
            emoji=profile.get("status_emoji", ""),
            text=profile.get("status_text", ""),
            expires=profile.get("status_expiration", False),
        )

    def to_profile(self) -> dict:
        return {
            "status_emoji": self.emoji,
            "status_text": self.text,
            "status_expiration": 0 if not self.expires else int(self.expires.timestamp()),
        }


class Slack:
    def __init__(self, token: str, msgr_sm):
        self.client = AsyncWebClient(token=token)
        self._desired_status = Status()
        msgr_sm.on_update.append(self._on_new_status)

    async def _on_new_status(self, status: MessengerStatus):
        self._desired_status = Status(
            emoji=f":{status.emoji}:" if status.emoji != "" else "",
            text=status.message,
            expires=False,
        )
        await self.set_status(self._desired_status)

    async def get_status(self) -> Optional[Status]:
        res = await self.client.users_profile_get()
        if not res.get("ok", False):
            return None
        return Status.from_profile(res["profile"])

    async def set_status(self, status: Status) -> bool:
        print(status.dict())
        res = await self.client.users_profile_set(profile=status.to_profile())
        return res.get("ok", False)


if __name__ == "__main__":
    import asyncio
    from os import environ
    slack = Slack(environ.get("SLACK_TOKEN"))
    async def update_status(slack: Slack):
        print(await slack.set_status(Status(
            emoji=":nerd_face:", text="Testing, One, Two …",
            expires=datetime.now(tz=timezone.utc) + timedelta(seconds=30),
        )))
        print(await slack.get_status())
    asyncio.run(update_status(slack))
