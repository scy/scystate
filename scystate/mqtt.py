import asyncio
from os import environ as env

import paho.mqtt.client as mqttc

from .event import Event


class MQTTConfigError(Exception):
    pass


class MQTT:
    def __init__(self):
        self.mqtt = mqttc.Client(protocol=mqttc.MQTTv5)
        if "MQTT_USER" in env:
            self.mqtt.username_pw_set(env["MQTT_USER"], env.get("MQTT_PASS", None))
        self.mqtt.max_queued_messages_set(20)
        try:
            self.host = env["MQTT_HOST"]
        except KeyError:
            raise MQTTConfigError()
        self.port = env.get("MQTT_PORT", 1883)
        self._main_loop = None

        self.on_connect = Event()
        self.on_message = Event()
        self.mqtt.on_connect = self._on_connect
        self.mqtt.on_message = self._on_message

        self.subscribe = self.mqtt.subscribe

    def _on_connect(self, client: mqttc.Client, userdata, flags, rc, props):
        self._main_loop.call_soon_threadsafe(self.on_connect, self, rc, flags)

    def _on_message(self, client: mqttc.Client, userdata, msg):
        self._main_loop.call_soon_threadsafe(self.on_message, self, msg.topic, msg.payload, msg.qos, msg.retain)

    def start(self):
        self._main_loop = asyncio.get_running_loop()
        self.mqtt.connect_async(self.host, self.port)
        self.mqtt.loop_start()
